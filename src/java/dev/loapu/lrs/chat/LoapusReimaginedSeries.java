package dev.loapu.lrs.chat;

import dev.loapu.lrs.chat.util.Lang;
import dev.loapu.lrs.chat.util.Settings;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class LoapusReimaginedSeries extends LoapusReimaginedSeriesPlugin
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private Settings settings = new Settings();
	private Lang lang = new Lang();
	private List<String> langList = new ArrayList<>();
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static LoapusReimaginedSeries i;
	public static LoapusReimaginedSeries get() { return i; }
	public LoapusReimaginedSeries() { i = this; }
	
	// -------------------------------------------- //
	// ENABLE
	// -------------------------------------------- //
	
	@Override
	public void onEnableInner()
	{
		loadResources();
		registerCommands();
		registerListeners();
		checkForUpdates();
	}
	
	// -------------------------------------------- //
	// METHODS
	// -------------------------------------------- //
	
	private void loadResources()
	{
		log("STARTED", "Loading resources...");
		log("STEP 1/3", "Loading plugin directories...");
		String folderPlugin = getDataFolder().getAbsolutePath();
		String folderLanguages = folderPlugin + File.separator + "lang";
		String folderSettings = folderPlugin + File.separator + "settings";
		List<File> fileList = new ArrayList<>();
		fileList.add(new File(folderPlugin));
		fileList.add(new File(folderLanguages));
		fileList.add(new File(folderSettings));
		for (File f : fileList)
		{
			if (f.mkdirs()) log("STEP 1/3", f.getName() + " directory does not exist and is therefore created.");
		}
		log("STEP 1/3", "All directories loaded successfully.");
		log("STEP 2/3", "Loading plugin settings...");
		loadSettings(true);
		log("STEP 2/3", "Successfully loaded " + settings.getSettingsMap().size() + " settings on version (" + settings.getVersion() + ").");
		log("STEP 3/3", "Loading plugin languages...");
		loadLang(langList, true);
		File[] fileList1 = Objects.requireNonNull(new File(folderLanguages).listFiles());
		List<File> langFiles = new ArrayList<>();
		StringBuilder sb = new StringBuilder();
		Lang tmpLang;
		for (File f : fileList1)
		{
			if (f.getName().contains("json"))
			{
				langFiles.add(f);
				tmpLang = Lang.read(f.getName().substring(0, 2));
				sb.append(tmpLang.getLocaleName()).append(" (v").append(tmpLang.getVersion()).append("), ");
			}
		}
		log("STEP 3/3", "There are " + langFiles.size() + " loaded languages: ");
		log("STEP 3/3", sb.toString().substring(0, sb.toString().length() - 2));
		log("FINISHED", "All resources loaded successfully.");
	}
	private void registerCommands()
	{
	
	}
	private void registerListeners()
	{
	
	}
	private void checkForUpdates()
	{
	
	}
	public Settings getSettings()
	{
		return settings;
	}
	private void settingsWatcher()
	{
		Bukkit.getScheduler().runTaskAsynchronously(this, () ->
		{
			Path settingsPath = Paths.get(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "Settings");
			try
			{
				WatchService watcher = settingsPath.getFileSystem().newWatchService();
				settingsPath.register(watcher, StandardWatchEventKinds.ENTRY_DELETE, StandardWatchEventKinds.ENTRY_MODIFY);
				
				WatchKey key;
				boolean scnd = false; //Let only every second watcher event fire!
				while ((key = watcher.take()) != null)
				{
					for (WatchEvent<?> event : key.pollEvents())
					{
						if (event.kind() == StandardWatchEventKinds.ENTRY_MODIFY && event.context().toString().equalsIgnoreCase("settings.json"))
						{
							if (scnd)
							{
								scnd = false;
							}
							else
							{
								
								log("File 'settings.json' was modified. Applying changes now...");
								this.settings = Settings.read();
								log("Changes successfully applied.");
								scnd = true;
							}
						}
						if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE && event.context().toString().equalsIgnoreCase("settings.json"))
						{
							loadSettings();
						}
					}
					key.reset();
					
				}
				
			}
			catch (Exception e)
			{
				error(e);
			}
		});
	}
	private void loadSettings() { loadSettings(false); }
	private void loadSettings(boolean watcher)
	{
		File settingsFile = new File(getDataFolder().getAbsolutePath() + File.separator + "settings" + File.separator + "settings.json");
		if (!settingsFile.exists())
		{
			try
			{
				Files.copy(getResource("settings" + File.separator + "settings.json"), settingsFile.toPath());
			}
			catch (IOException e)
			{
				error(e);
			}
		}
		Settings tmpRsSttngs = Settings.read(true);
		settings = Settings.read();
		if (!Objects.requireNonNull(tmpRsSttngs).getVersion().equalsIgnoreCase(settings.getVersion()))
		{
			settings = Settings.update();
		}
		if (watcher) settingsWatcher();
	}
	private void langWatcher()
	{
		File readme = new File(getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator + "README.txt");
		try
		{
			if (readme.exists()) Files.delete(readme.toPath());
			Files.copy(getResource("lang" + File.separator + "README.txt"), readme.toPath());
		}
		catch (IOException e)
		{
			error(e);
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(this, () ->
		{
			Path langPath = Paths.get(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "Languages");
			try
			{
				WatchService watcher = langPath.getFileSystem().newWatchService();
				langPath.register(watcher, StandardWatchEventKinds.ENTRY_DELETE);
				
				WatchKey key;
				while ((key = watcher.take()) != null)
				{
					for (WatchEvent<?> event : key.pollEvents())
					{
						if (event.kind() == StandardWatchEventKinds.ENTRY_DELETE && event.context().toString().equalsIgnoreCase("en.json"))
						{
							List<String> tempLangList = new ArrayList<>();
							tempLangList.add("en");
							loadLang(tempLangList);
							tempLangList.clear();
							log("Restored default language file. Please do not try to delete it, thank you.");
						}
					}
					key.reset();
				}
			}
			catch (Exception e)
			{
				error(e);
			}
		});
	}
	private void loadLang(List<String> lang) {	loadLang(lang, false); }
	private void loadLang(List<String> lang, boolean watcher)
	{
		if (lang.isEmpty()) return;
		String path = getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator;
		File langFile;
		for (String s : lang)
		{
			langFile = new File(path + s + ".json");
			this.debug("." + s + ".");
			if (!langFile.exists())
			{
				try
				{
					Files.copy(getResource("lang" + File.separator + s + ".json"), langFile.toPath());
				}
				catch (IOException e)
				{
					error(e);
				}
			}
		}
		
		for (String s : lang)
		{
			Lang tmpLangR = Lang.read(true, s);
			Lang tmpLangE = Lang.read(s);
			if (!Objects.requireNonNull(tmpLangR).getVersion().equalsIgnoreCase(tmpLangE.getVersion()))
			{
				Lang.update(s);
			}
		}
		
		this.lang = Lang.read("en");
		
		if (watcher) langWatcher();
	}
}
