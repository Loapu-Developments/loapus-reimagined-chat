/*
 * Copyright (c) 2019 Benjamin Selig. All Rights Reserved.
 */

package dev.loapu.lrs.chat.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.loapu.lrs.chat.LoapusReimaginedSeries;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class PluginFile
{
	private static LoapusReimaginedSeries plugin = LoapusReimaginedSeries.get();
	public static void write(String fileName, Object content)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		try {
			gson.toJson(content, new FileWriter(plugin.getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator + fileName));
		}
		catch (IOException e)
		{
			plugin.error(e);
		}
			/*try
			{
				BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter( new FileOutputStream(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator + fileName), StandardCharsets.UTF_8));
				fileWriter.write(jsonString);
				fileWriter.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}*/
	}
}
