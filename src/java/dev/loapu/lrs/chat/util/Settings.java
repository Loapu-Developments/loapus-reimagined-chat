/*
 * Copyright (c) 2019 Benjamin Selig. All Rights Reserved.
 */

package dev.loapu.lrs.chat.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.loapu.lrs.chat.LoapusReimaginedSeries;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Settings
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private String version;
	private LinkedHashMap<String, Object> settings = new LinkedHashMap<>();
	
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static Settings i;
	public static Settings get() { return i; }
	public Settings() { i = this; }
	
	// -------------------------------------------- //
	// GETTERS & SETTERS
	// -------------------------------------------- //
	
	public String getVersion() { return version; }
	
	public LinkedHashMap<String, Object> getSettingsMap() { return settings; }
	
	public String getString(String setting)
	{
		if (!settings.containsKey(setting))
		{
			return null;
		}
		
		return settings.get(setting).toString();
	}
	
	public boolean getBoolean(String setting)
	{
		if (!settings.containsKey(setting))
		{
			return false;
		}
		
		return Boolean.parseBoolean(settings.get(setting).toString());
	}
	
	public int getInt(String setting)
	{
		if (!settings.containsKey(setting))
		{
			return 0;
		}
		
		return Integer.parseInt(settings.get(setting).toString());
	}
	
	public void setSettingsMap(LinkedHashMap<String, Object> settings)
	{
		this.settings = settings;
		this.write();
	}
	
	public void set(String setting, Object value)
	{
		settings.put(setting, value);
		this.write();
	}
	
	public void write() { write("settings.json"); }
	
	public void write(String fileName)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		String jsonString = gson.toJson(this);
		try
		{
			FileWriter fileWriter = new FileWriter(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "settings" + File.separator + fileName);
			fileWriter.write(jsonString);
			fileWriter.close();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}
	
	public static Settings read() {	return read(false);	}
	
	public static Settings read(boolean resource)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		try
		{
			File tempFile = new File(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "settings" + File.separator + "tmp.json");
			if (!tempFile.exists()) { Files.copy(LoapusReimaginedSeries.get().getResource("settings" + File.separator + "settings.json"), tempFile.toPath()); }
			if (!resource) { tempFile =  new File(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "settings" + File.separator + "settings.json"); }
			BufferedReader br = new BufferedReader(new FileReader(tempFile));
			Settings tempSettings = gson.fromJson(br, Settings.class);
			Files.deleteIfExists(Paths.get(LoapusReimaginedSeries.get().getDataFolder().getAbsolutePath() + File.separator + "settings" + File.separator + "tmp.json"));
			return tempSettings;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static Settings update()
	{
		// Settings from Resources
		Settings tmpSettingsLocal = read(true);
		
		// Settings from File
		Settings tmpSettingsExternal = read();
		
		if (tmpSettingsExternal == null || tmpSettingsLocal == null) { return null;	}
		
		tmpSettingsExternal.write("settingsBackup.json");
		
		LinkedHashMap<String, Object> tmpSettingsMap = tmpSettingsLocal.getSettingsMap();
		
		//tmpSettingsMap.putAll(tmpSettingsExternal.getSettingsMap());
		
		for (Entry<String, Object> e : tmpSettingsExternal.getSettingsMap().entrySet())
		{
			if (!tmpSettingsMap.containsKey(e.getKey())) { continue; }
			tmpSettingsMap.put(e.getKey(), e.getValue());
		}
		
		tmpSettingsLocal.setSettingsMap(tmpSettingsMap);
		tmpSettingsLocal.write();
		return tmpSettingsLocal;
	}
}
