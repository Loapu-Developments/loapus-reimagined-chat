/*
 * Copyright (c) 2019 Benjamin Selig. All Rights Reserved.
 */

package dev.loapu.lrs.chat.util;

public enum Messages
{
	// -------------------------------------------- //
	// ENUMS
	// -------------------------------------------- //
	
	NO_PERMISSION,
	INCORRECT_USAGE,
	NOT_ENOUGH_ARGUMENTS,
	SPACER_LONG,
	SPACER_MEDIUM,
	SPACER_SHORT,
	SPACER_UPPER,
	SPACER_LOWER,
	CLOAPUSREIMAGINEDCHAT,
	ATTENTION,
	ADVICE,
}
