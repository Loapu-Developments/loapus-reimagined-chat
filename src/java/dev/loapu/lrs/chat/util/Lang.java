/*
 * Copyright (c) 2019 Benjamin Selig. All Rights Reserved.
 */

package dev.loapu.lrs.chat.util;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dev.loapu.lrs.chat.LoapusReimaginedSeries;
import org.bukkit.ChatColor;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

public class Lang
{
	// -------------------------------------------- //
	// FIELDS
	// -------------------------------------------- //
	
	private String version;
	private LinkedHashMap<String, String> locale = new LinkedHashMap<>();
	private LinkedHashMap<Messages, String> lang = new LinkedHashMap<>();
	private static LoapusReimaginedSeries plugin = LoapusReimaginedSeries.get();
	
	// -------------------------------------------- //
	// INSTANCE & CONSTRUCT
	// -------------------------------------------- //
	
	private static Lang i;
	public static Lang get() { return i; }
	public Lang() { i = this; }
	
	// -------------------------------------------- //
	// GETTERS & SETTERS
	// -------------------------------------------- //
	
	public String getVersion() { return version; }
	
	public String getLocaleCode() { return locale.get("code"); }
	
	public String getLocaleName() { return locale.get("name"); }
	
	private LinkedHashMap<Messages, String> getLangMap()
	{
		LinkedHashMap<Messages, String> tmp = new LinkedHashMap<>();
		
		for (Entry<Messages, String> e : this.lang.entrySet())
		{
			tmp.put(e.getKey(), e.getValue().replace("\\\\u0026", "&"));
		}
		
		return tmp;
	}
	
	private void setLangMap(LinkedHashMap<Messages, String> lang) { this.lang = lang; }
	
	public String getMessage(Messages message) { return getMessage(message, null); }
	
	public String getMessage(Messages message, String[] paramArrayOfString) {
		
		String rawStr;
		if (!lang.containsKey(message))
		{
			Lang tmp = this.getLocaleCode().equalsIgnoreCase("en") ? Lang.read(true, "en") : Lang.read("en");
			if (tmp == null) return null;
			rawStr = tmp.getLangMap().get(message);
		}
		else
		{
			rawStr = lang.get(message);
		}
		rawStr = rawStr.replaceAll("\\\\", "\\\\\\\\");
		String str = ChatColor.translateAlternateColorCodes('&', rawStr);
		
		if (paramArrayOfString == null) {
			
			return str;
			
		}
		
		if (paramArrayOfString.length == 0) {
			
			return str;
			
		}
		
		for (int i = 0; i < paramArrayOfString.length; i++) {
			
			str = str.replace("{" + i + "}", paramArrayOfString[i]);
			
		}
		
		return str;
		
	}
	
	public void write() { PluginFile.write(this.locale.get("code") + ".json", this); }
	
	public static Lang read(String locale) { return read(false, locale); }
	
	public static Lang read(boolean resource, String locale)
	{
		Gson gson = new GsonBuilder().setPrettyPrinting().serializeNulls().create();
		File filePath = new File(plugin.getDataFolder().getAbsolutePath() + File.separator + "lang");
		File[] listOfFiles = filePath.listFiles();
		boolean localeExists = false;
		assert listOfFiles != null: "Language files got deleted. Restart the server to solve the problem.";
		for (File f : listOfFiles)
		{
			if (f.getName().equalsIgnoreCase(locale + ".json")) localeExists = true;
		}
		if (!localeExists) locale = "en";
		try
		{
			File tempFile = new File(plugin.getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator + "tmp.json");
			if (!tempFile.exists()) { Files.copy(plugin.getResource("lang" + File.separator + locale + ".json"), tempFile.toPath()); }
			if (!resource) { tempFile =  new File(plugin.getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator + locale + ".json"); }
			BufferedReader br = new BufferedReader(new InputStreamReader( new FileInputStream(tempFile), StandardCharsets.UTF_8));
			Lang tempLang = gson.fromJson(br, Lang.class);
			Files.deleteIfExists(Paths.get(plugin.getDataFolder().getAbsolutePath() + File.separator + "lang" + File.separator + "tmp.json"));
			plugin.log(tempLang.getMessage(Messages.SPACER_UPPER) + " " + resource);
			return tempLang;
		}
		catch (IOException e)
		{
			e.printStackTrace();
			return null;
		}
	}
	
	public static Lang update(String locale)
	{
		// Settings from Resources
		Lang tmpLangLocal = Lang.read(true, locale);
		
		// Settings from File
		Lang tmpLangExternal = Lang.read(locale);
		
		if (tmpLangExternal == null || tmpLangLocal == null) { return null; }
		
		PluginFile.write(locale + ".backup", tmpLangExternal);
		
		LinkedHashMap<Messages, String> tmpLangMap = tmpLangLocal.getLangMap();
		
		//tmpSettingsMap.putAll(tmpSettingsExternal.getSettingsMap());
		
		for (Entry<Messages, String> e : tmpLangExternal.getLangMap().entrySet())
		{
			if (!tmpLangMap.containsKey(e.getKey())) { continue; }
			tmpLangMap.put(e.getKey(), e.getValue());
		}
		
		tmpLangLocal.setLangMap(tmpLangMap);
		tmpLangLocal.write();
		return tmpLangLocal;
	}
}
